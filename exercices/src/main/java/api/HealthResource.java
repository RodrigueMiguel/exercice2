package api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.MediaType;

@RequestMapping(value = HealthResource.HEALTH_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface HealthResource {
    
    String HEALTH_PATH = "/health";
    
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    String getHealth();
}
