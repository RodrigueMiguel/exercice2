package api;

public class StudentDto {
    
    public final String registrationNumber;
    public final String firstName;
    public final String lastName;
    public final String birthDate;
    public final String admissionDate;
    
    public StudentDto(String registrationNumber, String firstName, String lastName, String birthDate, String admissionDate) {
        this.registrationNumber = registrationNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.admissionDate = admissionDate;
    }
}
