package api;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RequestMapping(value = StudentResource.RESOURCE_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface StudentResource {
    
    String RESOURCE_PATH = "/students";
    String PATH_WITH_ID = "/{id}";
    String PARAM_ID = "id";
    
    @GetMapping
    List<StudentDto> get ();
    
    @GetMapping(PATH_WITH_ID)
    StudentDto get (@PathVariable(PARAM_ID) String id);

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    StudentDto create (@RequestBody StudentDto student);
    
}
