package controller;

import api.HealthResource;

public class HealthController  implements HealthResource{

    @Override
    public String getHealth() {
        return "Pong";
    }

}
